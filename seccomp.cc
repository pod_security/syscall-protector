// Copyright (C) 2019, Rory Bradford <roryrjb@gmail.com>
// MIT License

#include "seccomp.h"

namespace seccomp {

using v8::Context;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::NewStringType;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Value;

const char* ToCString(const v8::String::Utf8Value& value) {
  return *value;
}

Persistent<Function> NodeSeccomp::constructor;

NodeSeccomp::NodeSeccomp(scmp_filter_ctx ctx) : _ctx(ctx) {
}

NodeSeccomp::~NodeSeccomp() {
  seccomp_release(_ctx);
}

void NodeSeccomp::Init(Local<Object> exports) {
  Isolate* isolate = exports->GetIsolate();

  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(
      isolate, "NodeSeccomp", NewStringType::kNormal).ToLocalChecked());
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  NODE_SET_PROTOTYPE_METHOD(tpl, "init", SeccompInit);
  NODE_SET_PROTOTYPE_METHOD(tpl, "reset", SeccompReset);
  NODE_SET_PROTOTYPE_METHOD(tpl, "load", SeccompLoad);
  NODE_SET_PROTOTYPE_METHOD(tpl, "ruleAdd", SeccompRuleAdd);
  NODE_SET_PROTOTYPE_METHOD(tpl, "ruleAddExact", SeccompRuleAddExact);
  NODE_SET_PROTOTYPE_METHOD(tpl, "fileWriteWatchdog", SeccompFileWriteWatchdog);

  Local<Context> context = isolate->GetCurrentContext();
  constructor.Reset(isolate, tpl->GetFunction(context).ToLocalChecked());
  exports->Set(context, String::NewFromUtf8(
      isolate, "NodeSeccomp", NewStringType::kNormal).ToLocalChecked(),
               tpl->GetFunction(context).ToLocalChecked()).FromJust();
}

void NodeSeccomp::New(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();
  scmp_filter_ctx ctx;

  if (args.IsConstructCall()) {
    NodeSeccomp* obj = new NodeSeccomp(ctx);
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());
  } else {
    Local<Function> cons = Local<Function>::New(isolate, constructor);
    Local<Object> instance =
        cons->NewInstance(context).ToLocalChecked();
    args.GetReturnValue().Set(instance);
  }
}

void NodeSeccomp::SeccompInit(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  NodeSeccomp* obj = ObjectWrap::Unwrap<NodeSeccomp>(args.Holder());

   int err = prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0);
   if (err != 0) {
     printf("prctl error %d %s\n", err, strerror(-err));
   }
   if ((obj->_ctx = seccomp_init(args[0].As<v8::Number>()->Value())) == 0) {
    isolate->ThrowException(v8::Exception::TypeError(
      v8::String::NewFromUtf8(isolate, "Invalid argument.",
        v8::NewStringType::kNormal).ToLocalChecked()));
  } else {
    printf("after init\n");
    args.GetReturnValue().Set(args.This());
  }
}

void NodeSeccomp::SeccompReset(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  NodeSeccomp* obj = ObjectWrap::Unwrap<NodeSeccomp>(args.Holder());

   if ((seccomp_reset(obj->_ctx, args[0].As<v8::Number>()->Value())) != 0) {
    isolate->ThrowException(v8::Exception::TypeError(
      v8::String::NewFromUtf8(isolate, "Invalid argument.",
        v8::NewStringType::kNormal).ToLocalChecked()));
  }
}

void NodeSeccomp::SeccompLoad(const v8::FunctionCallbackInfo<v8::Value>& args) {
  NodeSeccomp* obj = ObjectWrap::Unwrap<NodeSeccomp>(args.Holder());
  seccomp_load(obj->_ctx);
}

void NodeSeccomp::SeccompRuleAddExact(const v8::FunctionCallbackInfo<v8::Value>& args) {
  v8::Isolate* isolate = args.GetIsolate();
  NodeSeccomp* obj = ObjectWrap::Unwrap<NodeSeccomp>(args.Holder());

  int type = args[0].As<v8::Number>()->Value();
  v8::String::Utf8Value syscall_str(isolate, args[1]);

  if (seccomp_rule_add_exact(
    obj->_ctx,
    type,
    seccomp_syscall_resolve_name(ToCString(syscall_str)),
    0
  ) != 0) {
    isolate->ThrowException(v8::Exception::TypeError(
      v8::String::NewFromUtf8(isolate, "Invalid argument.",
        v8::NewStringType::kNormal).ToLocalChecked()));
  }
}

void NodeSeccomp::SeccompRuleAdd(const v8::FunctionCallbackInfo<v8::Value>& args) {
  v8::Isolate* isolate = args.GetIsolate();
  NodeSeccomp* obj = ObjectWrap::Unwrap<NodeSeccomp>(args.Holder());

  int type = args[0].As<v8::Number>()->Value();
  v8::String::Utf8Value syscall_str(isolate, args[1]);

  if (seccomp_rule_add(
    obj->_ctx,
    type,
    seccomp_syscall_resolve_name(ToCString(syscall_str)),
    0
  ) != 0) {
    isolate->ThrowException(v8::Exception::TypeError(
      v8::String::NewFromUtf8(isolate, "Invalid argument.",
        v8::NewStringType::kNormal).ToLocalChecked()));
  } else {
    args.GetReturnValue().Set(args.This());
  }
}

std::string resolveSymbolicLink(const char* path) {
    std::string resolvedPath(path);
    char buf[PATH_MAX];
    ssize_t len;

    while ((len = readlink(resolvedPath.c_str(), buf, sizeof(buf) - 1)) != -1) {
        buf[len] = '\0';
        if (buf[0] == '/') {
            resolvedPath = buf;
        } else {
            resolvedPath = resolvedPath.substr(0, resolvedPath.find_last_of('/') + 1) + buf;
        }
        if (resolvedPath.compare(0, 5, "/proc") != 0) {
            break;
        }
    }

    return resolvedPath;
}

#include <string>

bool ends_with(const std::string& str, const std::string& suffix) {
    if (str.length() < suffix.length()) {
        return false;
    }
    return str.compare(str.length() - suffix.length(), suffix.length(), suffix) == 0;
}

void NodeSeccomp::SeccompFileWriteWatchdog(const v8::FunctionCallbackInfo<v8::Value>& args) {
    v8::Isolate* isolate = args.GetIsolate();
    NodeSeccomp* obj = ObjectWrap::Unwrap<NodeSeccomp>(args.Holder());
    std::string error_cant_malloc = "Can't allocate memory";
    std::string error_cant_assign_rule = "Can't assign rule to writable file";
    std::string error_cant_assign_default_rule = "Can't assign default rule";
    std::string error_cant_access_file = "Can't access file";


    for (int fd = 0; fd < 30; ++fd) {//FD_SETSIZE; ++fd) {
        // Check if FD is open
        if (fcntl(fd, F_GETFD) != -1) {
            struct stat statbuf;
            if (fstat(fd, &statbuf) == -1) {
                perror("fstat failed");
                continue;
            }
            // Check if it's a regular file
            if (S_ISREG(statbuf.st_mode)) {
                int flags = fcntl(fd, F_GETFL);
                if (flags == -1) {
                    perror("fcntl failed to get flags");
                    continue;
                }

                // Check if the file is open for writing or for both reading and writing
                if ((flags & O_ACCMODE) == O_WRONLY || (flags & O_ACCMODE) == O_RDWR) {
                char path[1024];
                snprintf(path, sizeof(path), "/proc/self/fd/%d", fd);
                char target_path[PATH_MAX];
                ssize_t len = readlink(path, target_path, sizeof(target_path) - 1);
                if (len != -1) {
                    target_path[len] = '\0';
                    printf("FD %d is a regular file: %s\n", fd, target_path);
                    int err = seccomp_rule_add(obj->_ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 1, SCMP_A0(SCMP_CMP_EQ, static_cast<scmp_datum_t>(fd))) ;
	            if (err != 0) {
			printf("error %d %s\n", err, strerror(-err));
			char errorMessage[PATH_MAX+256];
                        snprintf(errorMessage, sizeof(errorMessage), "2 Failed to assign rule for file descriptor %d: %s", fd, target_path);
                	isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, errorMessage, v8::NewStringType::kNormal).ToLocalChecked()));
                    }

                }
		}
            }
        }
    }
/*
    // Open the directory corresponding to the current process
    DIR *dir = opendir("/proc/self/fd");
    if (dir == NULL) {
        perror("opendir");
        return;
    }

    struct dirent *entry;
    char proc_fd_path[512]; // Path for /proc/self/fd
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_LNK) {
	  struct stat stat_buf;
	    snprintf(proc_fd_path, sizeof(proc_fd_path), "/proc/self/fd/%s", entry->d_name);
	    std::string target_path = resolveSymbolicLink(proc_fd_path);
	  if (ends_with(target_path, "/fd")) {
		  printf("got it! %s\n", target_path.c_str());
	  } else {
          if (fstat(atoi(entry->d_name), &stat_buf) == 0 && S_ISREG(stat_buf.st_mode)) {
                if (access(target_path.c_str(), W_OK) == 0) {
                    int fd = atoi(entry->d_name);
		    printf("File descriptor: %s\n", entry->d_name); // Debugging print
                    printf("Target path: %s\n", target_path.c_str()); // Debugging print

                    if (seccomp_rule_add(obj->_ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 1, SCMP_A0(SCMP_CMP_EQ, static_cast<scmp_datum_t>(fd))) != 0) {
			char errorMessage[512];
                        snprintf(errorMessage, sizeof(errorMessage), "2 Failed to assign rule for file descriptor %s: %s", entry->d_name, target_path.c_str());
                	isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, errorMessage, v8::NewStringType::kNormal).ToLocalChecked()));
                    }
                } else {
                	isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, error_cant_access_file.c_str(), v8::NewStringType::kNormal).ToLocalChecked()));
                }
	  }
	  }
        }
    }

    if (seccomp_rule_add(obj->_ctx, SCMP_ACT_ERRNO(EBADF), SCMP_SYS(write), 1, SCMP_A0(SCMP_CMP_NE, 1)) != 0) {
        isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, error_cant_assign_default_rule.c_str(), v8::NewStringType::kNormal).ToLocalChecked()));
    }
    closedir(dir);
    */
    args.GetReturnValue().Set(args.This());
}

}  // namespace seccomp
