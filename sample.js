const {
  SCMP_ACT_ALLOW,
  SCMP_ACT_ERRNO,
  NodeSeccomp,
  errors: {
    EADDRINUSE
  }
} = require('./')
const fs = require('fs');

const seccomp = NodeSeccomp()

//const fd = fs.openSync("tim", "w")
seccomp
  .init(SCMP_ACT_ALLOW)
//  .fileWriteWatchdog()
  .ruleAdd(SCMP_ACT_ERRNO(EADDRINUSE), 'bind')
  .load()

//const fd2 = fs.openSync("tim2", "w")
//fs.writeSync(fd2, "hello world")

require('http').createServer((req, res) => {
  res.end('hello\n')
}).listen(8000)

