{
  "targets": [
    {
      "target_name": "seccomp",
      "sources": [
        "seccomp.cc",
        "addon.cc",
      ],
      "libraries": [
        "-lseccomp"
      ]
    }
  ]
}
